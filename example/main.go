package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-sql/v2/pkg/sql"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

const topic = "counter"

type messagePayload struct {
	CounterUUID string `json:"counter_uuid"`
}

func publisher(pool *pgxpool.Pool) {
	logger := watermill.NewStdLogger(true, false)

	publisher, err := sql.NewPublisher(
		pool,
		sql.PublisherConfig{
			SchemaAdapter:        sql.DefaultPgxPoolSQLSchema{},
			AutoInitializeSchema: true,
		},
		logger,
	)
	checkErr(err)

	{
		tx, err := pool.BeginTx(context.Background(), pgx.TxOptions{IsoLevel: pgx.RepeatableRead})
		checkErr(err)

		publisher, err := sql.NewPublisher(
			tx,
			sql.PublisherConfig{
				SchemaAdapter:        sql.DefaultPgxPoolSQLSchema{},
				AutoInitializeSchema: true,
			},
			logger,
		)
		checkErr(err)

		payload, err := json.Marshal(messagePayload{
			CounterUUID: "never in database",
		})
		checkErr(err)

		msg := message.NewMessage(watermill.NewUUID(), payload)

		err = publisher.Publish(topic, msg)
		checkErr(err)

		tx.Rollback(context.Background())
	}

	go func() {
		for {
			payload, err := json.Marshal(messagePayload{
				CounterUUID: uuid.NewString(),
			})
			checkErr(err)

			msg := message.NewMessage(watermill.NewUUID(), payload)

			err = publisher.Publish(topic, msg)
			checkErr(err)

			time.Sleep(5 * time.Second)
		}
	}()
}

func consumer(pool *pgxpool.Pool) {
	logger := watermill.NewStdLogger(true, false)

	subscriber, err := sql.NewSubscriber(
		pool,
		sql.SubscriberConfig{
			SchemaAdapter:    sql.DefaultPgxPoolSQLSchema{},
			OffsetsAdapter:   sql.DefaultPgxPoolSQLOffsetsAdapter{},
			InitializeSchema: true,
		},
		logger,
	)
	checkErr(err)

	messager, err := subscriber.Subscribe(context.Background(), topic)
	checkErr(err)

	for msg := range messager {
		fmt.Printf("received message: %s, payload: %s\n", msg.UUID, string(msg.Payload))
		msg.Ack()
	}
}

func main() {
	pool := createDB()

	publisher(pool)

	consumer(pool)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func createDB() *pgxpool.Pool {
	pool, err := pgxpool.New(context.Background(), "postgres://watermill:password@localhost:5432/watermill")
	checkErr(err)

	err = pool.Ping(context.Background())
	checkErr(err)

	return pool
}
