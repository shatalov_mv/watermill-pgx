module gitlab.com/shatalov_mv/watermill-pgx

go 1.20

require (
	github.com/ThreeDotsLabs/watermill v1.3.3
	github.com/jackc/pgx/v5 v5.4.3
	github.com/oklog/ulid v1.3.1
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/lithammer/shortuuid/v3 v3.0.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
