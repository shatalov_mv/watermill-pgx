package sql

import (
	"fmt"
)

// DefaultPgxPoolSQLOffsetsAdapter is adapter for storing offsets in PostgreSQL database.
//
// DefaultPgxPoolSQLOffsetsAdapter is designed to support multiple subscribers with exactly once delivery
// and guaranteed order.
//
// We are using FOR UPDATE in NextOffsetQuery to lock consumer group in offsets table.
//
// When another consumer is trying to consume the same message, deadlock should occur in ConsumedMessageQuery.
// After deadlock, consumer will consume next message.
type DefaultPgxPoolSQLOffsetsAdapter struct {
	// GenerateMessagesOffsetsTableName may be used to override how the messages/offsets table name is generated.
	GenerateMessagesOffsetsTableName func(topic string) string
}

func (a DefaultPgxPoolSQLOffsetsAdapter) SchemaInitializingQueries(topic string) []string {
	return []string{`
		CREATE TABLE IF NOT EXISTS ` + a.MessagesOffsetsTable(topic) + ` (
		consumer_group VARCHAR(255) NOT NULL,
		offset_acked BIGINT,
		last_processed_transaction_id xid8 NOT NULL,
		PRIMARY KEY(consumer_group)
	)`}
}

func (a DefaultPgxPoolSQLOffsetsAdapter) NextOffsetQuery(topic, consumerGroup string) (string, []any) {
	return `SELECT 
    			coalesce(MAX(offset_acked),0) AS offset_acked, 
    			coalesce(MAX(last_processed_transaction_id::text), '0')::xid8 AS last_processed_transaction_id 
			FROM ` + a.MessagesOffsetsTable(topic) + ` 
			WHERE consumer_group=$1`,
		[]any{consumerGroup}
}

func (a DefaultPgxPoolSQLOffsetsAdapter) AckMessageQuery(topic string, row Row, consumerGroup string) (string, []any) {
	ackQuery := `INSERT INTO ` + a.MessagesOffsetsTable(topic) + `(offset_acked, last_processed_transaction_id, consumer_group) 
	VALUES 
		($1, $2, $3) 
	ON CONFLICT 
		(consumer_group) 
	DO UPDATE SET 
		offset_acked = excluded.offset_acked,
		last_processed_transaction_id = excluded.last_processed_transaction_id`
	return ackQuery, []any{row.Offset, row.ExtraData["transaction_id"], consumerGroup}
}

func (a DefaultPgxPoolSQLOffsetsAdapter) MessagesOffsetsTable(topic string) string {
	if a.GenerateMessagesOffsetsTableName != nil {
		return a.GenerateMessagesOffsetsTableName(topic)
	}
	return fmt.Sprintf(`"watermill_offsets_%s"`, topic)
}

func (a DefaultPgxPoolSQLOffsetsAdapter) ConsumedMessageQuery(topic string, row Row, consumerGroup string, consumerULID []byte) (string, []any) {
	return "", nil
}
