package sql

import (
	"context"
	"fmt"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/jackc/pgx/v5/pgconn"
)

func initializeSchema[T interface {
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
}](
	ctx context.Context,
	topic string,
	logger watermill.LoggerAdapter,
	db T,
	schemaAdapter SchemaAdapter,
	offsetsAdapter OffsetsAdapter,
) error {
	err := validateTopicName(topic)
	if err != nil {
		return err
	}

	initializingQueries := schemaAdapter.SchemaInitializingQueries(topic)
	if offsetsAdapter != nil {
		initializingQueries = append(initializingQueries, offsetsAdapter.SchemaInitializingQueries(topic)...)
	}

	logger.Info("Initializing subscriber schema", watermill.LogFields{
		"query": initializingQueries,
	})

	for _, q := range initializingQueries {
		_, err := db.Exec(ctx, q)
		if err != nil {
			return fmt.Errorf("cound not initialize schema | %w", err)
		}
	}

	return nil
}
